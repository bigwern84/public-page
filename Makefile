clean:
	rm -rf build/
build:
	mkdir -p build
	CGO_ENABLED=0 go build -o build/app -a -ldflags '-extldflags "-static" -s -w' app.go

